package com.company;

import java.util.Random;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random random = new Random();
        int randomNumber = random.nextInt(100 - 1);
        int attempt = 5;
        int difference=0;


        //System.out.println(randomNumber);
        System.out.println("Я загадал число от 1 до 100");
        System.out.println(String.format("У тебя есть %s попыток. Поехали!", attempt));

        while(attempt!=0){
            attempt--;
            int number=sc.nextInt();
            if(number==randomNumber){
                System.out.println(String.format("Ура, ты выйграл и сохранил %s попытки",attempt));
                break;
            } else {
                if (attempt==0){
                    System.out.println("Ты проиграл =(.\nВ следущий раз обязательно повезет");
                } else if(attempt==4){
                    difference=Math.abs(randomNumber-number);
                    System.out.println(String.format("Попробуй еще раз. У тебя %s попытки",attempt));
                } else {
                    int temp=Math.abs(randomNumber-number);
                    if(difference==temp){
                        System.out.println("Ты так же близок, как в предыдущий раз");
                    }if (difference>temp){
                        System.out.println("Теплее");
                    }if (difference<temp){
                        System.out.println("Холоднее");
                    }System.out.println(String.format("Попробуй еще раз, осталось %s попытки",attempt));
                }

            }
        }

    }
}








